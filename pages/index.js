import { useContext } from "react";
import { context } from "../pages/_app";
import removeTodoList from "../components/removeTodoList";
import Button from "../components/CreateButton";
import PopUp from "../components/popUp";
import ListTodoLists from "../components/ListTodoLists";
import ListTodos from "../components/ListTodos";
import addTodo from "../components/addTodo";

const Home = () => {
  const {
    todoLists,
    idSelected,
    todoListStartId,
    todoListCreatePage,
    setTodoListCreatePage,
    todoListModifyPage,
    setTodoListModifyPage,
    todoListCreateSubmit,
    todoListModifySubmit,
  } = useContext(context);

  const deleteTodoList = () => {
    removeTodoList(idSelected, todoLists, todoListStartId);
  };

  return (
    <>
      {/* header page principale */}

      <header>
        <nav className="flex border  border-solid border-transparent border-b-gray-300 gap-2 overflow-scroll scrollbar-hide">
          <ListTodoLists />
          <Button
            className="border rounded-t-xl px-3 py-2 border-solid border-gray-300 border-b-transparent"
            text="+"
            onClick={() => setTodoListCreatePage(true)}
          />
        </nav>
      </header>

      {/* éléments de modification des todos(List) */}

      <span className="flex justify-between border p-2 border-solid border-transparent border-b-gray-300 gap-2 overflow-scroll scrollbar-hide">
        <div>
          <Button className="rounded-xl px-3 py-2" text="➕"/>
            
          <Button
            className="rounded-xl px-3 py-2 "
            onClick={() => setTodoListModifyPage(true)}
            text = "🖊️"
          />
          <Button
            className="px-3 py-2"
            onClick={() => deleteTodoList()}
            text="🗑️"
          />
        </div>
        <input type="checkbox"></input>
      </span>

      <ListTodos />

      {/* popup création de todoList*/}

      <PopUp
        page={todoListCreatePage}
        setPage={setTodoListCreatePage}
        subText="Description"
        text="Create a new list"
        submit={todoListCreateSubmit}
      />

      <PopUp
        page={todoListModifyPage}
        setPage={setTodoListModifyPage}
        text="Edit list"
        subText="Description"
        submit={todoListModifySubmit}
      />
    </>
  );
};
export default Home;
