import { useContext } from "react";
import { context } from "../pages/_app";
import Button from "./CreateButton";

const ListTodos = () => {
  const { todoLists, todoStartId } = useContext(context);
  return (
    <div className="flex flex-col">
      {todoLists.map((todoList) =>
        todoList.todos.map((item) => (
          <span
            className="flex justify-between border p-2 border-solid border-transparent border-b-gray-300 gap-2 overflow-scroll scrollbar-hide"
            id={todoStartId + `${+item.id}`}
            key={item.id}
          >
              <div className="flex p-2 gap-4">
                <input type="checkbox"></input>
                <p>{item.title}</p>
              </div>
              <Button text="🗑️" />
          </span>
        ))
      )}
    </div>
  );
};

export default ListTodos;
