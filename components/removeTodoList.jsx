const removeTodoList = (id, todoLists, startId) => {
  const findTodoList = todoLists.find((todoList) => Number(todoList.id))
  if (findTodoList) {
    document.querySelector("#" + startId + String(id)).remove();
    return  todoLists.splice(
      findTodoList,
      1
    );
  }

  return console.log("not such a todo list")

};

export default removeTodoList;
